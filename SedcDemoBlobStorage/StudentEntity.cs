﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SedcDemoBlobStorage
{
    public class StudentEntity : TableEntity
    {
        public StudentEntity(string lastName, string firstName)
        {
            this.PartitionKey = lastName;
            this.RowKey = firstName;
        }

        public StudentEntity() { }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string School { get; set; }

        public string Country { get; set; }
    }
}
