﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types
using Microsoft.Azure;

namespace SedcDemoBlobStorage
{
    class Program
    {
        static void Main(string[] args)
        {
            //WriteToAppendBlob();

            //TableStorage.CreateTable();
            //var studentEntity = new StudentEntity("Trajkov", "Trajko")
            //{
            //    Country = "MKD",
            //    Email = "trajko@gmail.com",
            //    School = "SEDC",
            //    PhoneNumber = "888-999"
            //};
            //TableStorage.AddEntityToTable(studentEntity);

            //QueueStorage.CreateQueue();
            //QueueStorage.InsertMessageIntoQueue();
            //QueueStorage.PeekNextMessage();

            FileStorage.AccessFile();
        }

        public static void UploadABlobIntoContainer()
        {
            // Parse the connection string and return a reference to the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            var container = blobClient.GetContainerReference("sedc");
            container.CreateIfNotExists();

            var blockBlob = container.GetBlockBlobReference("sedc-blob");

            using (var fileStream = System.IO.File.OpenRead(@"samples\test.pdf"))
            {
                blockBlob.UploadFromStream(fileStream);
            }
        }

        public static void WriteToAppendBlob()
        {
            //Parse the connection string for the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                Microsoft.Azure.CloudConfigurationManager.GetSetting("StorageConnectionString"));

            //Create service client for credentialed access to the Blob service.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            //Get a reference to a container.
            CloudBlobContainer container = blobClient.GetContainerReference("my-append-blobs");

            //Create the container if it does not already exist.
            container.CreateIfNotExists();

            //Get a reference to an append blob.
            CloudAppendBlob appendBlob = container.GetAppendBlobReference("append-blob.log");

            //Create the append blob. Note that if the blob already exists, the CreateOrReplace() method will overwrite it.
            //You can check whether the blob exists to avoid overwriting it by using CloudAppendBlob.Exists().
            appendBlob.CreateOrReplace();

            int numBlocks = 10;

            //Generate an array of random bytes.
            Random rnd = new Random();
            byte[] bytes = new byte[numBlocks];
            rnd.NextBytes(bytes);

            //Simulate a logging operation by writing text data and byte data to the end of the append blob.
            for (int i = 0; i < numBlocks; i++)
            {
                appendBlob.AppendText(String.Format("Timestamp: {0:u} \tLog Entry: {1}{2}",
                    DateTime.UtcNow, bytes[i], Environment.NewLine));
            }
            
            //Read the append blob to the console window.
            Console.WriteLine(appendBlob.DownloadText());
        }
    }
}
